#例子

```javascript
/**
 * 例子 1
 * 实例化原始formModel类，添加设置规则
 * 优点：或许有些人会觉得比较容易理解
 */
var m = new formModel();
m.setVali([
    ["username", "require",      "请输入用户名。"],
    ["username", /^[_0-9a-z]$/g, "用户名只能由字母、数字和下划线组合。"]
]);

$("form").submit(function(){
    var pass = true;
    // 虽然是通过回调的方式，但其实并不是异步的
    m.check(formModel.serializeObject(this), function(err, data){
        if(err){
            alert(err.msg);
            pass = false;
        }
    });
    return pass;
});
```

```javascript
/**
 * 例子 2
 * 快速继承并实例化formModel
 * 优点：由于是继承的，对理解面向对象编程的人而言逻辑更加清晰，可以写方法，让复杂的验证使用callback时候更加方便
 */
var m = formModel.create({
    $v: function(){
        return [
            ["username", "require",      "请输入用户名。"],
            ["username", /^[_0-9a-z]$/g, "用户名只能由字母、数字和下划线组合。"]
        ];
    }
});

$("form").submit(function(){
    // 略
});
```

```javascript
/**
 * 例子 3
 * 继承formModel，编写新的类
 * 优点：更接近于面向对象编程，每个实例化对象都是独立的数据，对单页面多表单的情况下能更好的支持
 */
var Mod = formModel.extend({
    $v: function(){
        return [
            ["username", "require",      "请输入用户名。"],
            ["username", /^[_0-9a-z]$/i, "用户名只能由字母、数字和下划线组合。"]
        ];
    }
});
var m = new Mod();
$("form").submit(function(){
    // 略
});
```


#formModel

属性共包含如下几个方法：

##$a()

自动完成规则方法，该方法必须返回一个Object或Array，当实例化后，该方法会变成其返回的Object或Array，使用方法的形式返回Object或Array，是为了解决对象引用问题
```javascript
// 例子1
var Mod = formModel.extend({
    $a: function(){
        return {
            // 直接以字符串设置
            "sex" : "男",
            // 使用回调返回
            "password" : function(value, name){
                // 自动md5加密（假设存在md5方法）
                return md5(value);
            }
        }
    }
});
// 例子2
var Mod = formModel.extend({
    $a: function(){
        return [
            ["sex", "男", "string"],
            ["password", function(value, name){return md5(value);}, "function"],
            ["test1", "test2", "field"],
            ["test", "", "ignore"],
            {name: "test", action: "", type:"ignore"}
            // 可以使用array快速设置，也可以使用object完整设置
            // [*字段名, *完成操作, 操作方法]
            // {name:字段名, action:完成操作, type:操作方法}
            // 字段名  ：必填，表单name值
            // 完成操作：必填，根据type指定不同的操作
            // 操作方法: 可选，指定操作的类型，缺省"default"
            //              default   自动识别"string"和"callback"
            //              string    使用字符串
            //              callback  使用回调返回
            //              field     使用当前表单另一字段填充
            //              ignore    当值为空时忽略该字段
        ]
    }
});
```
##$m()

自动映射规则方法，该方法必须返回一个Object，当实例化后，该方法会变成其返回的Object，使用方法的形式返回Object，是为了解决对象引用问题
```javascript
var Mod = formModel.extend({
    $m: function(){
        return {
            "username" : "name",
            "password" : "pwd"
        }
    }
});
```
##$v()

自动验证规则方法，该方法必须返回一个Array，当实例化后，该方法会变成其返回的Array，使用方法的形式返回Array，是为了解决对象引用问题
```javascript
var Mod = formModel.extend({
    $v: function(){
        return [
            ["test", "require",     "不能为空", "regexp"],
            ["test", /[^_0-9a-z]/i, "不可以使用数字、字母和下划线以外的字符", "!regexp"],
            ["test", [6, 20],       "长度必须为6-20字节之间，一个汉字占用两个字节", "lengthAt"],
            ["test", ["0","1","2"], "只能是012。", "in"],
            ["test", "买家",        "只能是买家", "equal"],
            ["test", [6,16],        "密码长度必须为6-16字符之间", "length"],
            ["test", "test2",       "两次密码不匹配", "confirm"],
            ["test", [1,9],         "这个数字只能是1-9之间", "between"],
            ["test", "require",     "不管存不存在，这个必须填写", "regexp", Mod.MUST_VALIDATE]
            ["test", function(value, name){return (value!="1")}, "不能等于1", "callback"],
            {name:"test", action:"email", error:"邮箱地址不规范", type:"regex"}
            // 可以使用array快速设置，也可以使用object完整设置
            // [*字段名, *验证操作, *错误提示, 验证方法, 验证条件, 额外补充]
            // {name:字段名, action:验证操作, error:错误提示, type:验证方法, condition:验证条件, extra:额外补充}
            // 字段名  ：必填，表单name值
            // 验证操作：必填，根据type指定不同的操作，type缺省情况下为正则表达式验证
            // 错误提示：必填，验证不通过时候提示的错误信息
            // 验证方法：可选，指定操作的类型，缺省regexp
            //              支持 regexp、 callback、 confirm、 equal、 in、 length、 lengthAt、between
            //              反向!regexp、!callback、!confirm、!equal、!in、!length、!lengthAt、!between
            //              备注：反向情况下，结果取反
            //              regexp   正则表达式，支持使用字符串选择预设的正则表达式：
            //                           "require"   : /\S+/,    // 不为空
            //                           "email"     : /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,      // 邮箱
            //                           "url"       : /^http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?$/,   // 链接地址
            //                           "currency"  : /^(0|[1-9][0-9]*)(\.[0-9]{1,2})?$/,   // 货币
            //                           "number"    : /^\d+$/,                              // 数字
            //                           "zip"       : /^\d{6}$/,                            // 邮编
            //                           "integer"   : /^(0|-?[1-9][0-9]*)$/,                // 整数
            //                           "pinteger"  : /^(0|[1-9][0-9]*)$/,                  // 正整数
            //                           "double"    : /^-?(0|[1-9][0-9]*)\.[0-9]+$/,        // 浮点数
            //                           "pdouble"   : /^(0|[1-9][0-9]*)\.[0-9]+$/,          // 正浮点数
            //                           "english"   : /^[A-Za-z]+$/,                        // 英文字母
            //                           "chinese"   : /^[\u4e00-\u9fa5]+$/                  // 汉字
            //              callback 回调验证，函数，通过返回bool值以确认验证的通过性
            //              confirm  关联其它字段，例如注册表单中的确认密码可直接关联密码，对应的操作为密码的name值
            //              equal    等于某值
            //              in       在指定数组范围内
            //              length   字符长度
            //              lengthAt 字节长度，可使用extra额外补充设置一个汉字占用的字节数，缺省字节数为2
            //              between  验证数字的范围
            // 验证条件：可选，支持三种条件：
            //              0或formModel.EXISTS_VALIDATE   存在时验证
            //              1或formModel.MUST_VALIDATE     必须验证
            //              2或formModel.VALUE_VALIDATE    值不为空时验证
            // 额外补充：可选，用于个别验证操作需要的额外设置，暂时只有lengthAt用到
        ]
    }
});
```
##check(data, callback)

* data一个Object对象，需要传递的表单数据，其数据与服务器所接受的get、post一致
* callback 回调方法，将会接收到两个参数，分别为err、data
    * err   当验证没通过的时候，该参数会接收到错误对象，该对象包含name和msg属性，对应着引发错误的字段与错误信息
    * data  当所有验证都通过时候，该参数会是一个经过自动映射、自动完成后的Object数据
```javascript
var m = formModel.create({......});
$("form").submit(function(){
    m.check(formModel.serializeObject(this), function(err, data){
        if(err){
            alert(err.msg);
            $("input[name="+err.name+"]").focus();
        }else{
            $.ajax({
                url: "......",
                data: data      // 此处的data可以直接使用回调中接收到的data
                success: function(){......}
            })
        }
    });
    return false;
});
```
##initialize()

初始化方法，当你是面向对象的形式编写代码时候，如果有需要初始的数据，可使用此方法

##setAuto($a)

在外部设置自动完成规则，请注意，此处的$a参数与前面的$a方法不同，此处不需要传递函数，只要其对象即可
##setMap($m)

在外部设置自动映射规则，与setAuto同理
##setVali($v)

在外部设置自动验证规则，与setAuto同理

##$patch
布尔值，可以使得fromModel支持批量验证，当设置了$patch属性为true后，check中callback所接受的err参数的值将会是一个所有产生错误的数组集
提示：如果针对一个字段有多条验证规则，那么这个字段如果有其中一条验证没有通过，那么它后边的验证将终止而直接跳过下一个字段
```javascript
var m = formModel.create({
    $patch : true,
    $v: function(){
        return [......];    // 略
    }
});
$("form").submit(function(){
    m.check(formModel.serializeObject(this), function(err, data){
        if(err){
            // [
            //     {name:"字段1"， msg:"错误信息1"},
            //     {name:"字段2"， msg:"错误信息2"},
            //     {name:"字段3"， msg:"错误信息3"}
            //     
            // ]
            for(var i=0; i<ret.length; i++){
                alert(err[i].msg);
            }
        }
    });
    return false;
});
```

#formModel.extend(prototype)

静态方法：继承对象

#formModel.create(prototype)

静态方法：快速实例化对象

#formModel.EXISTS_VALIDATE;

静态数据：存在时验证

#formModel.MUST_VALIDATE;

静态数据：必须验证

#formModel.VALUE_VALIDATE;

静态数据：值不为空时验证


#children.parent

只针对继承的类有效，当继承过后，可以使用静态parent取到其所继承的类
例如：
```javascript
var Mod = formModel.extend({
    initialize: function(){
        // 调用其父亲的initialize方法
        Mod.parent.prototype.initialize.call(this);
        // this.constructor.parent.prototype.initialize.call(this); // 效果等同前面那句代码
    }
});
```
