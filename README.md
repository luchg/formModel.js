#支持自动映射、自动验证和自动完成的表单模型类
这是一款拥有类似于后端thinkphp的“三大自动”前端表单模型
```javascript
var RegModel = formModel.extend({
    $v: function(){
        return [
            ["username", "require", "请输入用户名。"],
            ["username", [6, null], "用户名长度过短，请限制在6-20个字节，1个汉字为2个字节。", "lengthAt"],
            ["username", [null,20], "用户名长度过长，请限制在6-20个字节，1个汉字为2个字节。", "lengthAt"],
            ["username", /\d{5}/,   "用户名中不能包含多个数字，推荐使用中文用户名。", "!regexp"],

            ["email",    "require", "请输入邮箱地址。"],
            ["email",    "email",   "邮箱地址格式有误。"],

            ["password", "require",            "请设置密码。"],
            ["password", /^[a-zA-Z]+$/,        "密码不能为纯字母。", "!regexp"],
            ["password", /^\d+$/,              "密码不能为纯数字。", "!regexp"],
            ["password", /^_+$/,               "密码不能为纯符号。", "!regexp"],
            ["password", /^[_0-9a-z]{6,20}$/i, "密码为6-20个字符，请使用字母加数字或下划线组合密码。"],

            ["repassword", "require",  "请输入确认密码。"],
            ["repassword", "password", "两次密码不匹配。", "confirm"]
        ]
    }
});
```
##License
formModel.js遵循[MIT](http://opensource.org/licenses/MIT)协议发布


***
##更新日志
### 1.2.1
* 新增error属性支持使用闭包设置动态错误信息，该闭包将接收到两个参数(name与value)，必须返回一个字符串

### 1.2.0
* 新增对批量验证的支持
* check方法优化，用法有变，name参数与err合并，解决设置“空错误信息”导致判断出错的问题，check用法详情[doc.md](doc.md)
```javascript
// 升级到1.2.0方法：由于check有变，因此原function(err, name, callback)变更为function(err, callback)，通过err.msg获取错误信息，err.name获取引发错误的字段
var m = formModel.create({......});
$("form").submit(function(){
    m.check(formModel.serializeObject(this), function(err, data){
        if(err){
            alert(err.msg);
            $("input[name="+err.name+"]").focus();
        }else{
            $.ajax({
                url: "......",
                data: data      // 此处的data可以直接使用回调中接收到的data
                success: function(){......}
            })
        }
    });
    return false;
});
```

### 1.1.0
* 更完善的“自动完成”功能
自动完成功能用法略有变化，但原版的用法依然支持，原版用法视为简洁法，新版支持使用数组进行更完善的设置，详情[doc.md](doc.md)
